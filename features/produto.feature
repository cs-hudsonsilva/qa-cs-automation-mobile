# language: pt
Funcionalidade: Produto
  Como um usuario normal
  Eu quero escolher a cor
  E o tamanho de um produto
  Para que eu possa adicionar o produto no Carrinho

  Contexto:
      Dado que cliquei em um banner de Catalogo na Tela Inicial
         E estou no Catalogo de Produtos
    Quando eu clico em um Produto
     Então tenho que ver os detalhes deste Produto

  Cenário: Adicionar produto no carrinho escolhendo tamanho e cor
    Quando eu clico no botao Tamanho para escolher o tamanho 40
         E clico no botao Cor para escolher a cor vermelha
         E eu clico no botao Adicionar no Carrinho
     Então tenho que ver o produto no Carrinho

# Dúvida: É problemático/ambiguo deixar a palavra tentar?
  Cenário: Falha ao tentar adicionar produto sem escolher Tamanho e Cor
    Quando eu clico no botao Adicionar no Carrinho
     Então vejo a mensagem de erro "Por favor selecione o tamanho e a cor"
#@dev
  Cenário: Falha ao tentar escolher a Cor antes de selecionar o Tamanho do Produto
    Quando eu clico no botao Cor antes de escolher o Tamanho do Produto
     Então vejo a mensagem de erro "Por favor selecione um tamanho"
