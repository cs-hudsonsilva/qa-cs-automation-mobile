Dado(/^que cliquei em um banner de Catalogo na tela inicial$/) do
  @pagina_inicial = page(TelaInicialScreen).await(timeout: 5)
  @pagina_inicial.clicar_em(@pagina_inicial.banner)
end

Dado(/^que estou no catalogo de produtos$/) do
  @catalogo = page(CatalogoScreen).await(timeout: 5)
  @catalogo.esta_visivel?(:layout_name)
end

Entao(/^tenho que ver ao menos um produto$/) do
  @catalogo.esta_visivel?(@catalogo.product)
end

Quando(/^eu clico em um produto$/) do
  @catalogo.clicar_em(@catalogo.product)
end

Entao(/^tenho que ver os detalhes deste produto$/) do
  @product = page(ProdutoScreen).await(timeout: 5)
  @product.esta_visivel?(@product.layout_name)
end
