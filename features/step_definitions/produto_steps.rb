## Contexto
Dado(/^que cliquei em um banner de Catalogo na Tela Inicial$/) do
  @pagina_inicial = page(TelaInicialScreen).await(timeout: 5)
  @pagina_inicial.clicar_em(@pagina_inicial.banner)
end

Dado(/^estou no Catalogo de Produtos$/) do
  @catalogo = page(CatalogoScreen).await(timeout: 5)
  @catalogo.esta_visivel?(:layout_name)
end

Quando(/^eu clico em um Produto$/) do
  @catalogo.clicar_em(@catalogo.product)
end

Entao(/^tenho que ver os detalhes deste Produto$/) do
  @product = page(ProdutoScreen).await(timeout: 5)
  @product.esta_visivel?(@product.layout_name)
end

# Cenarios

Quando(/^eu clico no botao Tamanho para escolher o tamanho (\d+)$/) do |tamanho|
  @product.drag_to 'baixo'.to_sym
  @product.clicar_em(@product.size_button)
  @product.clicar_em(@product.size_list_item)
end

Quando(/^clico no botao Cor para escolher a cor vermelha$/) do
# @product.drag_to 'baixo'.to_sym
  @product.clicar_em(@product.color_button)
  @product.clicar_em(@product.color_list_item)
end

Quando(/^eu clico no botao Adicionar no Carrinho$/) do
  @product.clicar_em(@product.cart_button)
end

Entao(/^tenho que ver o produto no Carrinho$/) do
  @carrinho = page(CarrinhoScreen).await(timeout: 5)
  @carrinho.esta_visivel?(@carrinho.layout_name)
end

Então(/^vejo a mensagem de erro "([^"]*)"$/) do |msg|
  @product.esta_visivel?(@product.id_message)
  @product.alerta_visivel?(msg)
end

Quando(/^eu clico no botao Cor antes de escolher o Tamanho do Produto$/) do
  @product.drag_to 'baixo'.to_sym
  @product.clicar_em(@product.color_button)
end
