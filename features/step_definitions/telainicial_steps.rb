######### DADO #########
Dado(/^que estou na Tela Inicial$/) do
  @page = page(TelaInicialScreen).await(timeout: 5)
end

Então(/^tenho que ver ao menos um banner$/) do
  fail 'Nao vai da nao..' if
    @page.existem_banners? == false
end
