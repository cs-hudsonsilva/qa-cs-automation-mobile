# language: pt
Funcionalidade: Tela Inicial
  A tela inicial deve conter ao menos um banner
  Se eu tocar em um banner, eu devo ver a tela de catálogo

  Cenário: Ao menos um banner na tela Inicial
     Dado que estou na Tela Inicial
    Então tenho que ver ao menos um banner
