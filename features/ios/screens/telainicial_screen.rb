class TelaInicialScreen < IOSScreenBase
  # Identificador da tela
  trait(:trait)                 { "* marked:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'HOME_SCREEN' }
  # element(:button)              { pending 'Insert button identificator' }

  element(:banner)              { 'BANNER_LIST' }
  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* marked:'#{button}'")
  # end

  def existem_banners?
    lista_banners = query("* id:'#{banner}'")
    return lista_banners.length >= 1
  end

  def clicar_em(element, query = nil)
    query = "* id:'#{element}'" if query.nil?
    begin
      wait_for(timeout: 5) { element_exists(query) }
      touch(query)
    rescue => e
      raise "Problem in touch screen element: '#{element}'\nError Message: #{e.message}"
    end
  end
end
