# language: pt
Funcionalidade: Catalogo
  Como um usuario normal
  Eu quero acessar ao catalogo de produtos
  E escolher um produto
  Para que eu possa ler os detalhes deste produto

  Contexto:
    Dado que cliquei em um banner de Catalogo na tela inicial
@dev
  Cenário: Ao menos um produto na tela de catalogo
     Dado que estou no catalogo de produtos
    Entao tenho que ver ao menos um produto
@dev
  Cenário: Ao clicar em um produto o usuario deve ver os detalhes deste produto
      Dado que estou no catalogo de produtos
    Quando eu clico em um produto
     Entao tenho que ver os detalhes deste produto
