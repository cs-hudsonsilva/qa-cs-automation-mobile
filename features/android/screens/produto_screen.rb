class ProdutoScreen < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'product_activity' }
  element(:size_button)         { 'button_size' }
  element(:color_button)        { 'button_color' }
  element(:size_list_item)      { 'text1' }
  element(:color_list_item)     { 'text1' }
  element(:cart_button)         { 'buy_button' }
  element(:id_message)          { 'message' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end
  def esta_visivel?(id, query = nil)
    query = "* id:'#{id}'" if query.nil?
    begin
      wait_for(timeout: 3) { element_exists query }
    rescue
      return false
    end
    true
  end

  def clicar_em(element, query = nil)
    query = "* id:'#{element}'" if query.nil?
    begin
      wait_for(timeout: 5) { element_exists(query) }
      touch(query)
    rescue => e
      raise "Problem in touch screen element: '#{element}'\nError Message: #{e.message}"
    end
  end

  def alerta_visivel?(msg, query = nil)
    query = "* {text CONTAINS[c] '#{msg}'}" if query.nil?
    begin
      wait_for(timeout: 3) { element_exists query }
    rescue
      return false
    end
    true
  end
end
