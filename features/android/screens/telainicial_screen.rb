class TelaInicialScreen < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)        { 'home_activity' }
  # element(:button)              { pending 'Insert button identificator' }
  element(:banner)             { 'category_home_item_layout' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end

  def existem_banners?
    lista_banners = query("* id:'#{banner}'")
    lista_banners.length >= 1
  end

  def clicar_em(element, query = nil)
    query = "* id:'#{element}'" if query.nil?
    begin
      wait_for(timeout: 5) { element_exists(query) }
      touch(query)
    rescue => e
      raise "Problem in touch screen element: '#{element}'\nError Message: #{e.message}"
    end
  end
end
